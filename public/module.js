(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.bundle = f()}})(function(){var define,module,exports;return (function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){

const roads = [
      "Дом Алисы - Дом Боба", "Дом Алисы - Склад",
      "Дом Алисы - Почта",    "Дом Боба - Ратуша",
      "Дом Дарии - Дом Эрни", "Дом Дарии - Ратуша",
      "Дом Эрни - Дом Греты", "Дом Греты - Ферма",
      "Дом Греты - Магазин",  "Рынок - Ферма",
      "Рынок - Почта",        "Рынок - Магазин",
      "Рынок - Ратуша",       "Магазин - Ратуша"
];


module.exports = roads;
const roadGraph = buildGraph(roads)

class VillageState {
      constructor(place, parcels){
            this.place = place;
            this.parcels = parcels;
      }

      move(destination){
            if (!roadGraph[this.place].includes(destination)){
                  return this;
            }else{
                  let parcels = this.parcels.map(p => {
                        if (p.place != this.place) return p;
                        return {place: destination, address: p.address};
                  }).filter(p => p.place != p.address);
                  return new VillageState(destination, parcels);
            }
      }
      static random(parcelCount){
            let parcels = [];
            for(let i = 0; i < parcelCount; i++){
                  let address = randomPick(Object.keys(roadGraph));
                  // address = 'Дом Алисы';
                  let place;
                  do {
                        place = randomPick(Object.keys(roadGraph));
                        // place = 'Рынок';
                  }while (place == address);
                  parcels.push({place, address});
            }
            return new VillageState("Почта", parcels);
      }
}
let randomParcel = VillageState.random(5);
battleOfRobots(goalOrientedRobot, diykstraRobot, 100, VillageState.random(5))
// runRobot(randomParcel, diykstraRobot, []);
// runRobot(randomParcel, goalOrientedRobot, [])
function battleOfRobots(robotA, robotB, testCycle, randomParcel){
      let robotAresult = [];
      let robotBresult = [];
      for (let i = 0; i < testCycle; i++){
            robotAresult.push(runRobot(randomParcel, robotA, []).turns);
            robotBresult.push(runRobot(randomParcel, robotB, []).turns);
      }
      let sumA = robotAresult.reduce((a, b) => a + b);
      let sumB = robotBresult.reduce((a, b) => a + b);
      let averageTurnsA = sumA / testCycle;
      let averageTurnsB = sumB / testCycle;
      console.log(`${robotA.name}: ${averageTurnsA} ходов`);
      console.log(`${robotB.name}: ${averageTurnsB} ходов`);
};

class PGroup {
      constructor(array){
            this.array = array;
      }
      static add(param){
            let bla = 0;
            if (this.array === undefined) this.array = [];
            this.array = this.array.concat(param);
            return new PGroup(this.array)
      }
      static delete(param){
            let bla = 0;
            if (this.array === undefined) return new PGroup(this.array);
            this.array = this.array.slice(param);
            return new PGroup(this.array)
      }
      static empty(param){
            add: this.add(param);
            let bla = 0;
            param = bla
            return new PGroup().add(param);

      }
      add(param){
            let bla = 0;
            if (this.array === undefined) this.array = [];
            this.array = this.array.concat(param);
            return new PGroup(this.array)
      }
      delete(param){
           // if (this.array === undefined) return new PGroup(this.array);
            let index = this.array.indexOf(param);
            if (index != -1){
                  this.array.splice(index, 1);
            }
            return new PGroup(this.array);
      }
}


// let a = PGroup.add([1]);
// PGroup.empty = new PGroup()
// let c = PGroup.empty.add("a");
// let ab = a.add("b");
// let b = ab.delete("a");





function passNode(graph, robotPlace){
      // let randomParcel = {};
      let routeMap = new Map();
      let checkedGraph = new Map(graph);
      let tempArr;
      let min;
      const entries = graph.entries();
      let size;
      let nextNodeMin;
      for (let k = 0; k < graph.size; k++){
            for(let i = 0; i < graph.get(robotPlace).sosedi.length; i++){
                  let minNode = getMinNode(checkedGraph, robotPlace);
                  let nextmetka = setNextMetka(graph, minNode, robotPlace);
            }
            if (tempArr === undefined) checkedGraph = new Map(graph);
            checkedGraph.delete(robotPlace);
            if (!checkedGraph.size){
                  let startNodeMetka = graph.get(randomParcel.place);
                  if (startNodeMetka.metka === Infinity){
                        startNodeMetka.metka = 0;
                        graph.set(randomParcel.place, startNodeMetka);
                  } 
                 break; 
            } 
            tempArr = [];graph
            nextNodeMin = checkedGraph.forEach((elem, index, arr) => {
                  tempArr.push([elem.metka, index]);
            }); 
            nextNodeMin = tempArr.map(elem => elem[0]).sort((a, b) => a - b)[0];
            robotPlace = tempArr.map(elem => elem).find((elem, index) => elem[0] == nextNodeMin)[1];
      }
      // console.log(graph);
      return new Map(graph);
      // return graph;
}

function setNextMetka(graph, minNodeMarked, robotPlace){
      let minArr = [];
      let minmin = minNodeMarked.sosedi.filter((elem, index, arr) => elem[2] === 'min')[0];
      let minNode = minNodeMarked.sosedi.filter((elem, index, arr) => elem[2] === 'min');
      minNodeMarked.sosedi.map((elem, index, arr) => {
            if (elem[2] === 'min') {
                  elem[2] = 'passed';
                  minArr.push(elem);
            }
      });
      let newMetka = '';
      for (let i = 0; i < minArr.length; i++){
            newMetka = graph.get(minArr[i][0]);
            if (newMetka !== undefined){
                  if (minNodeMarked.metka + minArr[i][1] < newMetka.metka){
                        newMetka.metka = minNodeMarked.metka + minArr[i][1]; 
                        if (newMetka.metka == 0 || newMetka.metka == Infinity){
                              newMetka.metka;
                        } 
                  } 
                  if (newMetka.metka == 0 || newMetka.metka == Infinity){
                              newMetka.metka;
                        } 
                  graph.set(minArr[i][0], newMetka);
            }else{
                  newMetka = graph.get(minArr[i][0]);
                  newMetka = graph.get(minArr[i][0]);
            }
            
      }
      // graph.set(robotPlace, minNodeMarked);
      return graph;
}

function getMinNode(graph, robotPlace){
      let currentNode = graph.get(robotPlace);
      let passed = currentNode.sosedi.map((elem) => {
            if (elem[2] !== 'passed') return elem;
      });
      let passedFiltered = passed.filter(elem => elem !== undefined);
      
      minEdgeToNode = passedFiltered.map((elem, index, arr) => elem[1]).sort((a, b) => a - b)[0];
      passedFiltered.map((elem, index) => {
            let flag = false;
            if (elem[1] == minEdgeToNode && flag == false){
                  elem.push('min');
                  flag = true;
            }
      });
      return currentNode;
}

function roadGraphAddWeight(roadGraph, robotPlace){
      let tempArr = [];
      let graphMap = new Map();
      for (let key in roadGraph){
            let val = roadGraph[key];
            for (let elem of val){
                  let randomEdge = Math.floor(Math.random() * (9 - 1 + 1) + 1)
                  // if (elem === 'Магазин') tempArr.push([elem, 2])
                  tempArr.push([elem, 1])
                  // tempArr.push([elem, randomEdge])
            }
            if (key === robotPlace){
                 graphMap.set(key, {'sosedi': tempArr, 'metka': 0}) 
           }else{
                  graphMap.set(key, {'sosedi': tempArr, 'metka': Infinity}) 
           }
            
            tempArr = [];      
      }
      return graphMap
}

function buildGraph(edges) {
      let graph = {};
      function addEdge(from, to){
            from = from.trim();
            to = to.trim();
            if (graph[from] == null) {
                  graph[from] = [to.trim()];
            }else{
                  graph[from].push(to.trim());
            }
      }
      for(let [from, to, a] of edges.map(r => r.split('-'))){
            addEdge(from, to);
            addEdge(to, from);
      }
      return graph;
}

function runRobot(state, robot, memory){
      let turns;
      for (let turn = 0;; turn++){
            if(state.parcels.length == 0){
                  // console.log(`выполнено за ${turn} ходов`)
                  turns = turn;
                  break;
            }
            if (robot(state, memory) === undefined){
                  debugger
            }
            let action = robot(state, memory);
            state = state.move(action.direction);
            memory = action.memory;
            // console.log(`Переход в направлении ${action.direction}`);
            if (turn > 200){
                  debugger
            }
      }
      let result = new Object();
      result.name = robot.name;
      result.turns = turns;
      return result;

}

function randomPick(array){
      let choice = Math.floor(Math.random() * array.length);
      return array[choice];
}

function randomRobot(state){
      return {
            direction: randomPick(roadGraph[state.place])
      }
}

function smartPick(tempArr){
      let choice;
      let minRoute;
      minRoute = tempArr.map(elem => elem[1].metka).sort((a, b) => a - b)[0];
      choice = tempArr.map(elem => elem).find((elem, index) => elem[1].metka == minRoute)[0];
      return choice;
}


function findRoute(graph, from, to){
      let work = [{at: from, route: []}];//начальная точка маршрута, массив точек маршрута
      for(let i = 0; i < work.length; i++){//цикл по точкам маршрута
            let {at, route} = work[i];
            for(let place of graph[at]){//если цель в соседях, идём туда
                  if (place == to) return route.concat(place);//цель в соседях
                  if (!work.some(w => w.at == place)){//если в маршруте нет
                        work.push({at: place, route: route.concat(place)});
                  }
            }
      }
}

function goalOrientedRobot({place, parcels}, route){
      if (route.length == 0){// если маршрут не построен
            let parcel = parcels[0];// выбираем первую посылку
            if (parcel.place != place){// если посылка лежит не там где мы строим маршрут к ней. Иначе относим
                  route = findRoute(roadGraph, place, parcel.place);
            }else{
                  route = findRoute(roadGraph, place, parcel.address);
            }
      }
      return {direction: route[0], memory: route.slice(1)};
}


function vumnyyRobot(state, memory){
      let robotPlace = state.place;
      graph = roadGraphAddWeight(roadGraph, robotPlace);
      passNode(graph, robotPlace);// перестраиваем граф из исходной точки
      let tempArrParcl = [];
      let tempArrAddr = [];
      let tempArr = [];
      /*сначала бегаем по всем местам, где лежат посылки, алгоритм Дийкстры.
        После того как собрали посылки, бегаем по местам доставки*/
      /*1. Место отправки есть в соседях узла?*/
      state.parcels.forEach((elem, index) => {
            tempArrParcl.push([elem.place, graph.get(elem.place)]);
      });

      function arrayEqual(arr1, arr2){
            arr1.sort((a, b) => a - b);
            arr2.sort((a, b) => a - b);
            let flag = true;
            if (arr1.length != arr2.length) return false;
            arr1.forEach((elem1, index1, arr1) => {
                  arr2.forEach((elem2, index2, arr2) => {
                        if(elem1 !== elem2) flag = false;
                  })
            });
            return flag;
      }
      if(tempArrAddr.length){
            tempArr = tempArrAddr;
            tempArrParcl = null;
      }else{
            tempArr = tempArrParcl;
            tempArrAddr = null;
      }
       /*- Минимальный путь к соседу*/
      let choice = smartPick(tempArr);
      /*- Если место назначения в соседях - идём туда*/
      if (graph.get(robotPlace).sosedi.find(elem => elem[0] === choice) && memory.indexOf(choice) == -1){
            /*- Если совпадает минимальный путь с местом отправки - идём туда*/
            memory.push(robotPlace);// добавляем всё пройденное в память
            return { 
                  direction: smartPick(tempArr),
                  memory: memory
            }
      }else{
            /*- Иначе исследуем Граф дальше по Дийкстре*/
            /*если уже*/
            tempArr = [];
            let flag = memory.find((elem => elem === '12'));
            graph.get(robotPlace).sosedi.forEach((elem, index) => {
                  if (!memory.find((memElem => memElem === elem[0]))){
                        tempArr.push([elem[0], graph.get(elem[0])])  
                  }
            });
            if (tempArr.length == 0){
                  graph.get(robotPlace).sosedi.forEach((elem, index) => {
                        if (memory.find((memElem => memElem === elem[0]))){
                              tempArr.push([elem[0], graph.get(elem[0])])
                              return;  
                        }
                  });
                  memory = [];
            }
            memory.push(robotPlace);// добавляем всё пройденное в память
            return {
               direction: smartPick(tempArr),
               memory: memory
            }
      }
}

// diykstraRobot(randomParcel, route)
function diykstraRobot({place, parcels}, route){
      let parcel = parcels[0];
      let robotPlace = place;
      let distanceToParcels = [];
      let jump = [];
      graph = roadGraphAddWeight(roadGraph, robotPlace);
      passNode(graph, robotPlace);// перестраиваем граф из исходной точки
      /*находим минимальное расстояние до посылки*/
      parcels.forEach((parcel, index) => {
            if (parcel.place === robotPlace) {
                  distanceToParcels.push({
                  parcelPlace: parcel.address,
                  distance: graph.get(parcel.address).metka});
            }else{
                  distanceToParcels.push({
                  parcelPlace: parcel.place,
                  distance: graph.get(parcel.place).metka});  
            }
      })
      /*если среди соседей*/
      if (distanceToParcels.some(parcel => parcel.distance == 1)){
            (distanceToParcels.some((parcel) => {
                  if (parcel.distance == 1){
                        return jump.push(parcel.parcelPlace);
                        route = jump;
                  }
            }))
            return {direction: jump[0], memory: jump.slice(1)};
      };

      /*если нет, идём к следующему минимальному узлу*/
      distanceToParcels.sort((a, b) => a.distance - b.distance); 
      /*ищем общих соседей у текущей позиции и след. минимального узла*/
      let sosediOfrobotPlace = graph.get(robotPlace).sosedi;
      let sosediOfNearlestParcel = graph.get(distanceToParcels[0].parcelPlace).sosedi;
      sosediOfrobotPlace.sort((a, b) => a[1] - b[1]);
      for (let sosed of sosediOfrobotPlace){
            if (sosediOfNearlestParcel.find(parcel => parcel[0] === sosed[0])){
                  jump.push(distanceToParcels[0].parcelPlace);
                  jump.push(sosed[0]);
                  route = jump;
                  return {direction: route[1], memory: route.slice(1)};  
            }
      };
      if (route.length != 0){
          return {direction: route[0], memory: route.slice(1)};  
      } 
      let startRobotPlace = robotPlace;
      do {
            for (let index = 0; index < sosediOfrobotPlace.length; index++){
                  robotPlace = sosediOfrobotPlace[index][0];
                  graphRoute = roadGraphAddWeight(roadGraph, robotPlace);
                  passNode(graphRoute, robotPlace);// перестраиваем граф из новой точки
                  /*определяем расстояние до посылки из новой точки*/
                  newDistanceToParcel = graphRoute.get(distanceToParcels[0].parcelPlace);
                  /*если сделали шаг в правильном направлении - записываем его в маршрут*/
                  if (newDistanceToParcel.metka < distanceToParcels[0].distance){
                        route.push(robotPlace);
                        /*находим соседей точки которую добавили в маршрут*/
                       sosediOfrobotPlace = graph.get(robotPlace).sosedi;
                  }
            }
      }while (route.length < distanceToParcels[0].distance);
      return {direction: route[1], memory: route.slice(1)};  
}

},{}]},{},[1])(1)
});
