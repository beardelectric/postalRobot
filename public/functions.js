
module.exports.buildGraph = function buildGraph(edges) {
	let graph = {};
	function addEdge(from, to){
		from = from.trim();
		to = to.trim();
		if (graph[from] == null) {
			graph[from] = [to.trim()];
		}else{
			graph[from].push(to.trim());
		}
	}
	for(let [from, to, a] of edges.map(r => r.split('-'))){
		addEdge(from, to);
		addEdge(to, from);
	}
	return graph;
}

function buildGraph(edges) {
	let graph = {};
	function addEdge(from, to){
		from = from.trim();
		to = to.trim();
		if (graph[from] == null) {
			graph[from] = [to.trim()];
		}else{
			graph[from].push(to.trim());
		}
	}
	for(let [from, to, a] of edges.map(r => r.split('-'))){
		addEdge(from, to);
		addEdge(to, from);
	}
	return graph;
}

module.exports.runRobot = function runRobot(state, robot, memory){
	for (let turn = 0;; turn++){
		if(state.parcels.length == 0){
			console.log(`выполнено за ${turn} ходов`)
			break;
		}
		let action = robot(state, memory);
		state = state.move(action.direction);
		memory = action.memory;
		console.log(`Переход в направлении ${action.direction}`);
		
	}
}

module.exports.randomPick = function randomPick(array){
	let choice = Math.floor(Math.random() * array.length);
	return array[choice];
}

function randomPick(array){
	let choice = Math.floor(Math.random() * array.length);
	return array[choice];
}

module.exports.randomRobot = function randomRobot(state){
	return {
		direction: randomPick(main.roadGraph[state.place])
	}
}

// module.exports